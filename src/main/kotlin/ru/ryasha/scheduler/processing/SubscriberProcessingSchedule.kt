package ru.ryasha.scheduler.processing

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import ru.ryasha.scheduler.service.SubscriberProcessingService
import ru.ryasha.scheduler.service.UnavailableSubscriberService

/**
 * Компонент с методами-обработчиками записей абонентов, работающих по расписаниям.
 */
@EnableScheduling
@Component
class SubscriberProcessingSchedule @Autowired constructor(
    private val subscriberService: UnavailableSubscriberService,
    private val processingService: SubscriberProcessingService
) {

    companion object {
        private val logger = LoggerFactory.getLogger(SubscriberProcessingSchedule::class.java)
    }

    /**
     * Выполняет ping-запросы по расписанию, указанному в конфигурации.
     */
    @Scheduled(fixedDelayString = "#{T(Long).parseLong(\${scheduler.ping.minutes}) * 60 * 1000}")
    fun pingSubscribers() {
        try {
            val subscribers = subscriberService.findNotPingedSubscribers()

            if (subscribers.isNotEmpty()) {
                logger.info("Starting the ping process...")
                subscribers.forEach {
                    logger.debug("Performing ping of the subscriber {}", it)
                    it.successfulPing = try {
                        processingService.pingSubscriber(it)
                    } catch (e: Exception) {
                        logger.error("An error occurred while trying to ping the subscriber {}.", it, e)
                        false
                    }
                    logger.debug("A subscriber's ping status: {}", it.successfulPing)
                }

                subscriberService.saveAllSubscribers(subscribers)
                logger.info("Processed subscribers: {}", subscribers.size)
            } else {
                logger.info("No subscribers to ping.")
            }
        } catch (e: Exception) {
            logger.error("An error occurred while performing ping process.", e)
        }
    }

    /**
     * Выполняет отправку сообщений звонящим по расписанию, указанному в конфигурации.
     */
    @Scheduled(cron = "\${scheduler.message-sender.cron}")
    fun sendMessagesToCallers() {
        try {
            val subscribers = subscriberService.findPingedSubscribers()
            val subscribersToRemove = mutableListOf<Long>()

            if (subscribers.isNotEmpty()) {
                logger.info("Sending messages...")
                subscribers.forEach {
                    logger.debug("Sending message to the subscriber {}", it)
                    val messageSent = try {
                        processingService.sendMessageToCaller(it)
                    } catch (e: Exception) {
                        logger.error("An error occurred while sending message to the subscriber {}.", it, e)
                        false
                    }

                    if (messageSent) {
                        logger.debug("A message was successfully sent to the subscriber {}.", it)
                        subscribersToRemove.add(it.id)
                    } else {
                        logger.debug("A message wasn't sent to the subscriber {}.", it)
                    }
                }

                subscriberService.deleteSubscribersById(subscribersToRemove)
                logger.info("Messages successfully sent: {}", subscribersToRemove.size)
            } else {
                logger.info("No subscribers waiting for a message.")
            }
        } catch (e: Exception) {
            logger.error("An error occurred while sending messages.", e)
        }
    }

    /**
     * Выполняет чистку данных в хранилище по расписанию, указанному в конфигурации.
     */
    @Scheduled(cron = "\${scheduler.purge.cron}")
    fun performDataPurge() {
        try {
            logger.info("Removing old data...")
            subscriberService.deleteOldData()
            logger.info("Old data has been removed.")
        } catch (e: Exception) {
            logger.error("An error occurred while removing old data.", e)
        }
    }
}