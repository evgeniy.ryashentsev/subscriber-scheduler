package ru.ryasha.scheduler.mapper

/**
 * Интерфейс мапперов Entity в DTO и наоборот.
 *
 * @param E класс Entity.
 * @param D класс DTO.
 */
interface EntityMapper<E, D> {

    /**
     * Маппинг из DTO в Entity.
     *
     * @param dto DTO с данными.
     * @return заполненная данными Entity.
     */
    fun mapToEntity(dto: D) : E

    /**
     * Маппинг из Entity в DTO.
     *
     * @param entity Entity с данными.
     * @return заполненный данными DTO.
     */
    fun mapFromEntity(entity: E) : D
}