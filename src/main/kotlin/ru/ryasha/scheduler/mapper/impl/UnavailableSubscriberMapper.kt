package ru.ryasha.scheduler.mapper.impl

import org.springframework.stereotype.Component
import ru.ryasha.scheduler.dto.UnavailableSubscriberDto
import ru.ryasha.scheduler.mapper.EntityMapper
import ru.ryasha.scheduler.model.UnavailableSubscriber
import java.time.LocalDateTime

@Component
class UnavailableSubscriberMapper : EntityMapper<UnavailableSubscriber, UnavailableSubscriberDto> {

    override fun mapToEntity(dto: UnavailableSubscriberDto): UnavailableSubscriber {
        return UnavailableSubscriber(msisdnA = dto.msisdnA, msisdnB = dto.msisdnB, creationTime = LocalDateTime.now())
    }

    override fun mapFromEntity(entity: UnavailableSubscriber): UnavailableSubscriberDto {
        return UnavailableSubscriberDto(entity.msisdnA, entity.msisdnB)
    }
}