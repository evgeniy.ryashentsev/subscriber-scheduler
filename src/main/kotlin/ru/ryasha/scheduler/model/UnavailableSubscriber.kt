package ru.ryasha.scheduler.model

import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

/**
 * Сущность с информацией по недоступному абоненту.
 *
 * @property id Идентификатор записи.
 * @property msisdnA Номер звонящго.
 * @property msisdnB Номер, на который производился звонок, когда он был недоступен.
 * @property creationTime Время создания записи.
 * @property successfulPing Флаг успешности ping-запроса.
 */
@Entity
class UnavailableSubscriber(
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    val id: Long = 0,
    val msisdnA: String,
    val msisdnB: String,
    val creationTime: LocalDateTime,
    var successfulPing: Boolean = false
) : Serializable