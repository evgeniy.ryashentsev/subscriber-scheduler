package ru.ryasha.scheduler.repository

import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import ru.ryasha.scheduler.model.UnavailableSubscriber
import java.time.LocalDateTime

/**
 * Репозиторий записей о недоступных абонентах.
 */
interface UnavailableSubscriberRepository : JpaRepository<UnavailableSubscriber, Long> {

    /**
     * Получает все записи, имеющие неудачный статус ping-запроса.
     */
    fun findAllBySuccessfulPingIsFalse() : List<UnavailableSubscriber>

    /**
     * Получает все записи, имеющие успешный статус ping-запроса.
     *
     * @param pageable ограничитель количества читаемых записей.
     * @return список записей, имеющие успешный статус ping-запроса.
     */
    fun findAllBySuccessfulPingIsTrue(pageable: Pageable) : List<UnavailableSubscriber>

    /**
     * Удаляет записи, которые старше указанной даты.
     *
     * @param beforeDate дата-маркер. Записи, которые старше этой даты, подлежат удалению.
     */
    fun deleteUnavailableSubscribersByCreationTimeBefore(beforeDate: LocalDateTime)

    /**
     * Удаляет записи с указанными ID.
     *
     * @param ids список ID к удалению.
     */
    fun deleteByIdIn(ids: List<Long>)
}