package ru.ryasha.scheduler.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import ru.ryasha.scheduler.dto.UnavailableSubscriberDto
import ru.ryasha.scheduler.mapper.EntityMapper
import ru.ryasha.scheduler.model.UnavailableSubscriber
import ru.ryasha.scheduler.service.SubscriberProcessingService

/**
 * Предоставляет маппинг для регистрации недоступных абонентов.
 */
@RestController
@RequestMapping("/unavailableSubscriber")
class UnavailableSubscriberRegistrationController @Autowired constructor(
    private val processingService: SubscriberProcessingService,
    private val entityMapper: EntityMapper<UnavailableSubscriber, UnavailableSubscriberDto>
) {

    companion object {
        private val logger = LoggerFactory.getLogger(UnavailableSubscriberRegistrationController::class.java)
    }

    /**
     * Метод для регистрации недоступных абонентов.
     *
     * @param dto транспортный объект с информацией о недоступном абоненте.
     */
    @PostMapping
    fun registerUnavailableSubscriber(@RequestBody dto: UnavailableSubscriberDto) : ResponseEntity<String> {
        return try {
            processingService.performInitialProcessing(entityMapper.mapToEntity(dto))
            ResponseEntity.ok("OK")
        } catch (e: Exception) {
            logger.error("An error occurred while processing unavailable subscriber {}.", dto, e)
            ResponseEntity<String>("Internal server error. Message: ${e.message}", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}