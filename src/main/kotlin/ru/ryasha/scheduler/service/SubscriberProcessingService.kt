package ru.ryasha.scheduler.service

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.ryasha.scheduler.dto.MessageRequestDto
import ru.ryasha.scheduler.dto.PingStatus
import ru.ryasha.scheduler.feign.MessageSenderClient
import ru.ryasha.scheduler.feign.SubscriberPingClient
import ru.ryasha.scheduler.model.UnavailableSubscriber
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Сервис, предосталвяющий методы для процессинга абонентов.
 */
interface SubscriberProcessingService {

    /**
     * Выполняет ping-запрос недоступного абонента.
     *
     * @param subscriber информация об абонентах.
     * @return true, если запрос завершился успешно. Иначе - false.
     */
    fun pingSubscriber(subscriber: UnavailableSubscriber): Boolean

    /**
     * Выполняет начальную обработку поступившего запроса.
     *
     * @param subscriber информация об абонентах.
     */
    fun sendMessageToCaller(subscriber: UnavailableSubscriber): Boolean

    /**
     * Выполняет начальную обработку поступившего запроса.
     *
     * @param subscriber информация об абонентах.
     */
    fun performInitialProcessing(subscriber: UnavailableSubscriber)
}

@Service
class SubscriberProcessingServiceImpl @Autowired constructor(
    private val pingClient: SubscriberPingClient,
    private val senderClient: MessageSenderClient,
    private val subscriberService: UnavailableSubscriberService,
    @Value("\${scheduler.message-sender.messageTemplate}") val messageTemplate: String
) : SubscriberProcessingService {

    companion object {
        private val DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM hh:mm:ss")
        private val logger = LoggerFactory.getLogger(SubscriberProcessingServiceImpl::class.java)
    }

    override fun pingSubscriber(subscriber: UnavailableSubscriber): Boolean {
        return try {
            val pingResponseDto = pingClient.pingSubscriber(subscriber.msisdnB)
            logger.debug("Ping status: {} for subscriber {}.", pingResponseDto.status, subscriber)
            PingStatus.SUCCESS == PingStatus.fromStatus(pingResponseDto.status)
        } catch (e: Exception) {
            logger.error("Ping failed.", e)
            false
        }
    }

    override fun sendMessageToCaller(subscriber: UnavailableSubscriber): Boolean {
        val messageText = messageTemplate.format(DATE_TIME_FORMATTER.format(LocalDateTime.now()))
        val messageDto = MessageRequestDto(subscriber.msisdnA, subscriber.msisdnB, messageText)

        return try {
            senderClient.sendMessage(messageDto)
            logger.debug("The message was sent to the subscriber {}.", subscriber)
            true
        } catch (e: Exception) {
            logger.debug("The message wasn't sent to the subscriber {}.", subscriber, e)
            false
        }
    }

    /**
     * Выполняет начальную обработку поступившего запроса.
     *
     * 1. Выполняется ping-запрос:<br>
     *     * Запрос завершился успешно, переход к шагу 2;<br>
     *     * Запрос завершился неудачей, выполняется сохранение данных. Конец сценария;<br>
     * 2. Выполняется отправка сообщения:<br>
     *     * Запрос завершился успешно. Конец сценария;<br>
     *     * Запрос завершился неудачей, выполняется сохранение данных. Конец сценария.<br>
     *
     * @param subscriber информация об абонентах.
     */
    override fun performInitialProcessing(subscriber: UnavailableSubscriber) {
        if (!pingSubscriber(subscriber)) {
            logger.debug("Initial process: unsuccessful ping. Saving subscriber {}...", subscriber)
            subscriberService.saveUnavailableSubscriber(subscriber)
            return
        }

        if (!sendMessageToCaller(subscriber)) {
            logger.debug("Initial process: message wasn't sent. Saving subscriber {}...", subscriber)
            subscriber.successfulPing = true
            subscriberService.saveUnavailableSubscriber(subscriber)
            return
        }

        logger.debug("The initial processing has been finished successfully for the subscriber {}", subscriber)
    }

}