package ru.ryasha.scheduler.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.transaction.annotation.Transactional
import ru.ryasha.scheduler.model.UnavailableSubscriber
import ru.ryasha.scheduler.repository.UnavailableSubscriberRepository
import java.time.LocalDateTime

/**
 * Сервис, предоставляющий методы для работы с записями [UnavailableSubscriber].
 */
interface UnavailableSubscriberService {

    /**
     * Добавляет запись в хранилище.
     *
     * @param subscriber объект [UnavailableSubscriber].
     */
    fun saveUnavailableSubscriber(subscriber: UnavailableSubscriber)


    /**
     * Добавляет в хранилище список записей.
     *
     * @param subscribers список объектов [UnavailableSubscriber].
     */
    fun saveAllSubscribers(subscribers: List<UnavailableSubscriber>)

    /**
     * Удаляет записи по их ID.
     *
     * @param ids список ID записей.
     */
    fun deleteSubscribersById(ids: List<Long>)

    /**
     * Удаляет "устаревшие" данные.
     */
    fun deleteOldData()

    /**
     * @return список абонентов, ping которых не прошел.
     */
    fun findNotPingedSubscribers() : List<UnavailableSubscriber>

    /**
     * @return список абонентов, ping которых прошел.
     */
    fun findPingedSubscribers() : List<UnavailableSubscriber>
}

@Service
@EnableTransactionManagement
open class UnavailableSubscriberServiceImpl @Autowired constructor(
    private val repository: UnavailableSubscriberRepository,
    @Value("#{T(Integer).parseInt(\${scheduler.message-sender.max-message-count})}") private val maxMessageCount: Int,
    @Value("#{T(Integer).parseInt(\${scheduler.purge.time-to-live-days})}") private val timeToLiveDays: Long
) : UnavailableSubscriberService {

    override fun saveUnavailableSubscriber(subscriber: UnavailableSubscriber) {
        repository.save(subscriber)
    }

    override fun saveAllSubscribers(subscribers: List<UnavailableSubscriber>) {
        repository.saveAll(subscribers)
    }

    @Transactional
    override fun deleteSubscribersById(ids: List<Long>) {
        repository.deleteByIdIn(ids)
    }

    /**
     * Удаляет "устаревшие" данные.
     *
     * Использует [timeToLiveDays] для определения "устаревания".
     */
    override fun deleteOldData() {
        repository.deleteUnavailableSubscribersByCreationTimeBefore(LocalDateTime.now().minusDays(timeToLiveDays))
    }

    override fun findNotPingedSubscribers(): List<UnavailableSubscriber> {
        return repository.findAllBySuccessfulPingIsFalse()
    }

    /**
     * @return список абонентов, ping которых прошел.
     *
     * Использует [maxMessageCount] для ограничения количества одновременно
     * обрабатываемых сообщений.
     */
    override fun findPingedSubscribers(): List<UnavailableSubscriber> {
        return repository.findAllBySuccessfulPingIsTrue(PageRequest.of(0, maxMessageCount))
    }

}