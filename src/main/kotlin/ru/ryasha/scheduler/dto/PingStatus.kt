package ru.ryasha.scheduler.dto

/**
 * Перечисление возможных статусов ping-запроса.
 */
enum class PingStatus(val status: String) {

    SUCCESS("inNetwork"),
    FAILURE("unavailableSubscriber");

    companion object {

        @JvmStatic
        fun fromStatus(value: String) :PingStatus {
            return values().firstOrNull { it.status == value } ?: FAILURE
        }
    }
}