package ru.ryasha.scheduler.dto

/**
 * Транспортный объект с информацией о результатах ping-запроса.
 *
 * @property status Статус ping-запроса.
 */
data class PingResponseDto(
        val status: String
)

/**
 * Транспортный объект, содержащий информацию для отправки SMS-соообщения абоненту.
 *
 * @property msisdnA Номер звонившего.
 * @property msisdnB Номер, на который производился звонок, когда он был недоступен.
 * @property text Текст сообщения.
 */
data class MessageRequestDto(
        val msisdnA: String,
        val msisdnB: String,
        val text: String
)

/**
 * Транспортный объект с информацией о недоступном абоненте.
 *
 * @property msisdnA Номер звонившего.
 * @property msisdnB Номер, на который производился звонок, когда он был недоступен.
 */
data class UnavailableSubscriberDto(
        val msisdnA: String,
        val msisdnB: String
)